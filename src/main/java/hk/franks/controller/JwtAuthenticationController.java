package hk.franks.controller;

import hk.franks.config.JwtTokenUtil;
import hk.franks.entity.User;
import hk.franks.exception.AuthenticationException;
import hk.franks.model.JwtRequest;
import hk.franks.model.response.APIResponse;
import hk.franks.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(value = "/authenticate", consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<APIResponse<String>> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        try {
            User user = authenticationService.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
            return ResponseEntity.ok(new APIResponse<>(APIResponse.CODE_SUCCESS, jwtTokenUtil.generateToken(user) ));
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new APIResponse<>(APIResponse.CODE_FAILED, e.getMessage(), null ));
        }

    }
}
