package hk.franks.model.response;

import lombok.Data;

@Data
public class APIResponse<T> {

    public static final Integer CODE_SUCCESS = 0;
    public static final Integer CODE_FAILED = -1;

    public APIResponse() {
        super();
    }

    public APIResponse(Integer code, String message, T payload) {
        super();
        this.code = code;
        this.message = message;
        this.payload = payload;
    }

    public APIResponse(Integer code, T payload) {
        super();
        this.code = code;
        this.payload = payload;
    }

    Integer code ;
    String message ;
    T payload;


}
