package hk.franks.service;

import hk.franks.entity.User;
import hk.franks.exception.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

	public User authenticate(String userName, String password) throws AuthenticationException {
		if ("frank".equalsIgnoreCase(userName)) {
			return new User("abc", "abc@abc.abc");
		} else {
			throw new AuthenticationException("User not exist or password incorrect.");
		}
	}

	public User loadUserByUsername(String userName) throws AuthenticationException {
		if ("frank".equalsIgnoreCase(userName)) {
			return new User("abc", "abc@abc.abc");
		} else {
			throw new AuthenticationException("User not exist or password incorrect.");
		}
	}

}