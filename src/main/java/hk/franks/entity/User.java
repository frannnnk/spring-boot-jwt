package hk.franks.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class User {
    String username;
    String email;

    public User() {
        super();
    }

    public User(String userName, String email) {
        super();
        this.username = userName;
        this.email = email;
    }

    public Map<String, Object> toMap(){
        Map<String , Object> map = new HashMap<>();
        map.put("username", username);
        map.put("email", email);
        return map;
    }

    public static User fromMap(Map<String , Object> map){
        User user = new User();
        user.setUsername((String)map.get("username"));
        user.setEmail((String)map.get("email"));
        return user;
    }
}
